# CSV Converter
This app is made to convert an input(messy) csv file to an output(regular) csv file. It provides an endpoint which recives an input csv file to be converted and in response gives back converted file for downloading.


## Table of contents
* [Technologies](#technologies)
* [Status](#status)
* [Additional information](#additional-info)
* [Contact](#contact)


## Technologies
* Spring Framework
* Junit 4
* Maven

__Also used__
* Java 11
* Lombok
* Swagger

## Status
Project is: finished.

## Additional information

Supports only one structure of csv file as in input.csv(you can check it out in resources folder).

## Contact
Created by [Klaudia Petrus](https://www.linkedin.com/in/klaudiapetrus/) - feel free to contact me on linkedin!
