package com.klaudia.csvconverter.service.mapper;

import com.klaudia.csvconverter.model.Bike;
import com.klaudia.csvconverter.service.BikeDto;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class BikeMapperTest {
    private BikeMapper bikeMapper = new BikeMapper();

    @Test
    public void mapBikeFromBikeDTOTestingFirst7Fields() {
        BikeDto bikeDto
                = BikeDto.builder()
                .numericModelID(5555)
                .marketingModelNumber(6666)
                .modelName("cool")
                .modelYear(2016)
                .textIdentifier("textId")
                .rearSuspension("rear suspension")
                .frontSuspension("front suspension")
                .build();

        Bike bike = bikeMapper.mapBikeDtoToModel(bikeDto);

        assertEquals(bikeDto.getMarketingModelNumber(), bike.getMarketingModelNumber());
        assertEquals(bikeDto.getNumericModelID(), bike.getNumericModelID());
        assertEquals(bikeDto.getModelName(), bike.getModelName());
        assertEquals(bikeDto.getModelYear(), bike.getModelYear());
        assertEquals(bikeDto.getRearSuspension(), bike.getRearSuspension());
        assertEquals(bikeDto.getFrontSuspension(), bike.getFrontSuspension());
        assertEquals(bikeDto.getTextIdentifier(), bike.getTextIdentifier());
    }

    @Test
    public void mapBikeFromBikeDTOTestingRearListLines() {
        BikeDto bikeDto
                = BikeDto.builder()
                .numericModelID(5555)
                .marketingModelNumber(6666)
                .modelName("cool")
                .modelYear(2016)
                .textIdentifier("textId")
                .rearSuspension("rear suspension")
                .frontSuspension("front suspension")
                .rear(addSampleLinesList("rear"))
                .build();

        Bike bike = bikeMapper.mapBikeDtoToModel(bikeDto);

        assertFalse(bike.getRearSpring().getRangeStringMap().isEmpty());
        assertFalse(bike.getRearShockStroke().getRangeStringMap().isEmpty());
        assertFalse(bike.getRearRebound().getRangeStringMap().isEmpty());
        assertFalse(bike.getRearShockSagPSI().getRangeStringMap().isEmpty());
        assertTrue(bike.getRearSpring().getRangeStringMap().containsKey(bikeDto.getRear().get(0)[0]));
        assertTrue(bike.getRearRebound().getRangeStringMap().containsKey(bikeDto.getRear().get(0)[0]));
        assertTrue(bike.getRearShockSagPSI().getRangeStringMap().containsKey(bikeDto.getRear().get(0)[0]));
        assertTrue(bike.getRearShockStroke().getRangeStringMap().containsKey(bikeDto.getRear().get(0)[0]));
    }

    @Test
    public void mapBikeFromBikeDTOTestingFrontListLines() {
        BikeDto bikeDto
                = BikeDto.builder()
                .numericModelID(5555)
                .marketingModelNumber(6666)
                .modelName("cool")
                .modelYear(2016)
                .textIdentifier("textId")
                .rearSuspension("rear suspension")
                .frontSuspension("front suspension")
                .front(addSampleLinesList("front"))
                .build();

        Bike bike = bikeMapper.mapBikeDtoToModel(bikeDto);

        assertFalse(bike.getFrontSpring().getRangeStringMap().isEmpty());
        assertFalse(bike.getFrontForSag().getRangeStringMap().isEmpty());
        assertFalse(bike.getFrontRebound().getRangeStringMap().isEmpty());
        assertTrue(bike.getFrontSpring().getRangeStringMap().containsKey(bikeDto.getFront().get(0)[0]));
        assertTrue(bike.getFrontRebound().getRangeStringMap().containsKey(bikeDto.getFront().get(0)[0]));
        assertTrue(bike.getFrontForSag().getRangeStringMap().containsKey(bikeDto.getFront().get(0)[0]));
    }

    @Test
    public void mapBikeFromBikeDTOTestingEmptyBikeDto() {
        BikeDto bikeDto
                = BikeDto.builder()
                .build();

        Bike bike = bikeMapper.mapBikeDtoToModel(bikeDto);

        assertEquals(bikeDto.getMarketingModelNumber(), bike.getMarketingModelNumber());
        assertEquals(bikeDto.getNumericModelID(), bike.getNumericModelID());
        assertEquals(bikeDto.getModelName(), bike.getModelName());
        assertEquals(bikeDto.getModelYear(), bike.getModelYear());
        assertEquals(bikeDto.getRearSuspension(), bike.getRearSuspension());
        assertEquals(bikeDto.getFrontSuspension(), bike.getFrontSuspension());
        assertEquals(bikeDto.getTextIdentifier(), bike.getTextIdentifier());
    }


    private List<String[]> addSampleLinesList(String s) {
        List<String[]> sampleFrontLinesList = new ArrayList<>();
        int min = 0;
        int max = 100;
        for (int i = 0; i < 16; i++) {
            if (i == 1) {
                min += 101;
            } else {
                min += 100;
            }
            max += 100;
            String[] line;
            if (s.equals("front")) {
                line = getFrontLines(i, min, max);
            } else {
                line = getRearLines(i, min, max);
            }
            sampleFrontLinesList.add(line);
        }
        return sampleFrontLinesList;
    }

    private String[] getRearLines(int i, int min, int max) {
        //101-110,105,PSI,7,clicks out,45,mm,12,mm
        String[] line = {min + "-" + max, String.valueOf(i), "bla",
                String.valueOf(i + 20), "hello", String.valueOf(i + 5), "hi", String.valueOf(i + 1), "hey"};
        return line;
    }

    private String[] getFrontLines(int i, int min, int max) {
        //0-100,48,PSI,3,clicks out,18,mm,,
        String[] line = {min + "-" + max, String.valueOf(i), "bla",
                String.valueOf(i + 1), "hello", String.valueOf(i + 3), "hi", "",};

        return line;
    }
}