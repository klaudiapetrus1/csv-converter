package com.klaudia.csvconverter.controller;

import com.klaudia.csvconverter.model.Bike;
import com.klaudia.csvconverter.service.BikeCreator;
import com.klaudia.csvconverter.service.BikeDtoCreator;
import com.klaudia.csvconverter.service.mapper.BikeMapper;
import com.klaudia.csvconverter.utils.BikeFieldNames;

import java.io.*;
import java.util.List;


public class MainController {
    public static void main(String[] args) {
        ParseModelToCsvFile parseModelToCsvFile = new ParseModelToCsvFile();
        ParseCSVLineByLine parseCSVLineByLine = new ParseCSVLineByLine(new BikeDtoCreator(), new BikeCreator(new BikeMapper()));

        InputStream csvInputStream = ClassLoader.getSystemResourceAsStream("input.csv");

        List<Bike> bikeList = parseCSVLineByLine.parseFromFileToModel(csvInputStream);

        File file = parseModelToCsvFile.saveBikesToCsvFile("output.csv", bikeList, true);

    }


}
