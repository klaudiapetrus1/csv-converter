package com.klaudia.csvconverter.controller;

import com.klaudia.csvconverter.model.Bike;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.List;


@RestController
@RequestMapping("/api/csv")
@RequiredArgsConstructor
public class CSVController {
    private final ParseCSVLineByLine parseCSVLineByLine;
    private final ParseModelToCsvFile parseModelToCsvFile;

    @PostMapping("/convert-csv-file")
    public ResponseEntity convertCsvFile(@RequestParam("file") MultipartFile file) {
        if (!file.isEmpty()) {
            try {
                List<Bike> bikeList = parseCSVLineByLine.parseFromFileToModel(file.getInputStream());
                File csvFile = parseModelToCsvFile.saveBikesToCsvFile("output.csv", bikeList, true);

                return ResponseEntity.ok()
                        .header("Content-Disposition", "attachment; filename=" + "output.csv")
                        .contentLength(csvFile.length())
                        .contentType(MediaType.parseMediaType("text/css"))
                        .body(new FileSystemResource(csvFile));

            } catch (Exception e) {
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("Could not convert the file!");
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Please upload a csv file!");
    }

}
