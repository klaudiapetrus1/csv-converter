package com.klaudia.csvconverter.controller;

import com.klaudia.csvconverter.model.Bike;
import com.klaudia.csvconverter.service.BikeCreator;
import com.klaudia.csvconverter.service.BikeDto;
import com.klaudia.csvconverter.service.BikeDtoCreator;
import com.klaudia.csvconverter.utils.KeyWords;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Service
@RequiredArgsConstructor
public class ParseCSVLineByLine {
    private final BikeDtoCreator bikeDtoCreator;
    private final BikeCreator bikeCreator;

    public List<Bike> parseFromFileToModel(InputStream csvInputStream) {
        List<List<String[]>> bikesFromCSV = new ArrayList<>();
        List<String[]> listOfLines = new ArrayList<>();
        String line;
        String[] lineElements;
        List<Bike> bikeList = new ArrayList<>();
        if (csvInputStream != null) {

            try (InputStreamReader streamReader = new InputStreamReader(csvInputStream, StandardCharsets.UTF_8);
                 BufferedReader reader = new BufferedReader(streamReader)) {

                while ((line = reader.readLine()) != null) {
                    lineElements = line.split(",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)");

                    if (lineElements.length > 0) {
                        if (lineElements[0].equalsIgnoreCase(KeyWords.BIKE_MODEL.toString())) {
                            listOfLines = new ArrayList<>();
                        }
                        if (lineElements.length < 2 && lineElements[0].equalsIgnoreCase(KeyWords.Text_Identifier.toString())) {
                            lineElements = new String[]{KeyWords.Text_Identifier.toString(), null};
                        }
                        if (!lineElements[0].equalsIgnoreCase(KeyWords.BIKE_MODEL.toString()) && !lineElements[0].equalsIgnoreCase(KeyWords.END_BIKE_MODEL.toString())) {
                            listOfLines.add(lineElements);
                        }
                        if (lineElements[0].equalsIgnoreCase(KeyWords.END_BIKE_MODEL.toString())) {
                            bikesFromCSV.add(listOfLines);
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            List<BikeDto> bikeDtoList = bikeDtoCreator.createBikeDtoListFromCsvFileBikes(bikesFromCSV);
            bikeList = bikeCreator.createBikeListFromDto(bikeDtoList);
        }
        return bikeList;
    }


}


