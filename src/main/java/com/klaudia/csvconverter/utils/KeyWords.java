package com.klaudia.csvconverter.utils;


public enum KeyWords {
    BIKE_MODEL("BIKE MODEL"),
    Text_Identifier("Text Identifier"),
    END_BIKE_MODEL("END BIKE MODEL");

    private final String word;

    KeyWords(String s) {
        word = s;
    }

    public String toString() {
        return this.word;
    }

}
