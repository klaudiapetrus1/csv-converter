package com.klaudia.csvconverter.utils;


import com.klaudia.csvconverter.service.BikeDto;

public enum BikeFieldNames {
    Numeric_Model_ID {
        @Override
        public void setData(BikeDto bikeDto, String[] line) {
             bikeDto.setNumericModelID(Integer.valueOf(line[1]));
        }
    },
    Marketing_Model_Number {
        @Override
        public void setData(BikeDto bikeDto, String[] line) {
            bikeDto.setMarketingModelNumber(Integer.valueOf(line[1]));
        }
    },
    Model_Year {
        @Override
        public void setData(BikeDto bikeDto, String[] line) {
            bikeDto.setModelYear(Integer.valueOf(line[1]));
        }
    },
    Model_Name {
        @Override
        public void setData(BikeDto bikeDto, String[] line) {
            bikeDto.setModelName(line[1]);
        }
    },
    Text_Identifier {
        @Override
        public void setData(BikeDto bikeDto, String[] line) {
            if(line[1] != null) {
                bikeDto.setTextIdentifier(line[1]);
            } else {
                bikeDto.setTextIdentifier("");
            }
        }
    },
    Travel {
        @Override
        public void setData(BikeDto bikeDto, String[] line) {
            bikeDto.setTravel(line[1]);
        }
    },
    Front_Suspension {
        @Override
        public void setData(BikeDto bikeDto, String[] line) {
            bikeDto.setFrontSuspension(line[1]);
        }
    },
    Rear_Suspension {
        @Override
        public void setData(BikeDto bikeDto, String[] line) {
            bikeDto.setRearSuspension(line[1]);
        }
    };

    public abstract void setData(BikeDto bikeDto, String[] line);

}
