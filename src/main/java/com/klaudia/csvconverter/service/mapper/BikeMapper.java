package com.klaudia.csvconverter.service.mapper;

import com.klaudia.csvconverter.service.BikeDto;
import com.klaudia.csvconverter.model.*;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class BikeMapper {

    public Bike mapBikeDtoToModel(BikeDto bikeDto) {
        FrontForSag frontForSag = new FrontForSag();
        FrontRebound frontRebound = new FrontRebound();
        FrontSpring frontSpring = new FrontSpring();

        if (bikeDto.getFront() != null) {
            mapFrontLines(bikeDto.getFront(), frontSpring, frontRebound, frontForSag);
        }

        RearSpring rearSpring = new RearSpring();
        RearRebound rearRebound = new RearRebound();
        RearShockStroke rearShockStroke = new RearShockStroke();
        RearShockSagPSI rearShockSagPSI = new RearShockSagPSI();

        if (bikeDto.getRear() != null) {
            mapRearLines(bikeDto.getRear(), rearSpring, rearRebound, rearShockStroke, rearShockSagPSI);
        }

        return Bike.builder()
                .numericModelID(bikeDto.getNumericModelID())
                .marketingModelNumber(bikeDto.getMarketingModelNumber())
                .modelYear(bikeDto.getModelYear())
                .modelName(bikeDto.getModelName())
                .textIdentifier(bikeDto.getTextIdentifier())
                .travel(bikeDto.getTravel())
                .frontSuspension(bikeDto.getFrontSuspension())
                .rearSuspension(bikeDto.getRearSuspension())
                .frontSpring(frontSpring)
                .frontForSag(frontForSag)
                .frontRebound(frontRebound)
                .rearRebound(rearRebound)
                .rearShockStroke(rearShockStroke)
                .rearShockSagPSI(rearShockSagPSI)
                .rearSpring(rearSpring)
                .build();
    }

    private void mapRearLines(List<String[]> rearList, RearSpring rearSpring, RearRebound rearRebound,
                              RearShockStroke rearShockStroke, RearShockSagPSI rearShockSagPSI) {

        for (String[] line : rearList) {
            String range = line[0];
            rearSpring.getRangeStringMap().put(range, line[1] + " " + line[2]);
            rearRebound.getRangeStringMap().put(range, line[3] + " " + line[4]);
            rearShockStroke.getRangeStringMap().put(range, line[5] + " " + line[6]);
            rearShockSagPSI.getRangeStringMap().put(range, line[7] + " " + line[8]);
        }
    }

    private void mapFrontLines(List<String[]> frontList, FrontSpring frontSpring, FrontRebound frontRebound,
                               FrontForSag frontForSag) {
        for (String[] line : frontList) {
            String range = line[0];
            frontSpring.getRangeStringMap().put(range, line[1] + " " + line[2]);
            frontRebound.getRangeStringMap().put(range, line[3] + " " + line[4]);
            frontForSag.getRangeStringMap().put(range, line[5] + " " + line[6]);
        }
    }


}
