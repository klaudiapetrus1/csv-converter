package com.klaudia.csvconverter.service;

import com.klaudia.csvconverter.utils.BikeFieldNames;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BikeDtoCreator {

    public List<BikeDto> createBikeDtoListFromCsvFileBikes(List<List<String[]>> bikeFromCSVList) {

        return bikeFromCSVList.stream()
                .map(bikeFromCsv -> createNewBikeDto(bikeFromCsv))
                .collect(Collectors.toList());
    }

    public BikeDto createNewBikeDto(List<String[]> bikeFromCsv) {
        BikeDto bikeDto = BikeDto.builder().build();
        bikeFromCsv.stream()
                .filter(bikeLine -> !bikeLine[0].equalsIgnoreCase("Pounds"))
                .forEach( bikeLine-> setDataToBikeDto(bikeLine, bikeDto));
        return bikeDto;
    }

    private void setDataToBikeDto(String[] bikeLine, BikeDto bikeDto){
        if (bikeLine.length > 2) {
            if (bikeLine.length < 8) {
                bikeDto.getFront().add(bikeLine);
            } else {
                bikeDto.getRear().add(bikeLine);
            }
        } else {
            BikeFieldNames.valueOf(bikeLine[0].replace(' ', '_')).setData(bikeDto, bikeLine);
        }
    }

}
