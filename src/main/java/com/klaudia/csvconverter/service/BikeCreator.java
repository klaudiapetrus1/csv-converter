package com.klaudia.csvconverter.service;

import com.klaudia.csvconverter.model.Bike;
import com.klaudia.csvconverter.service.mapper.BikeMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BikeCreator {
    private final BikeMapper mapper;

    public List<Bike> createBikeListFromDto(List<BikeDto> bikeDtoList) {

        return bikeDtoList.stream()
                .map(bikeDto -> mapper.mapBikeDtoToModel(bikeDto))
                .collect(Collectors.toList());
    }
}
