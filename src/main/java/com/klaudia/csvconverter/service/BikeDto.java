package com.klaudia.csvconverter.service;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BikeDto {
    private Integer numericModelID;
    private Integer marketingModelNumber;
    private Integer modelYear;
    private String modelName;
    private String textIdentifier;
    private String travel;
    private String frontSuspension;
    private String rearSuspension;
    @Builder.Default
    private List<String[]> front = new ArrayList<>();
    @Builder.Default
    private List<String[]> rear = new ArrayList<>();
}
