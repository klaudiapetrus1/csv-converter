package com.klaudia.csvconverter.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Bike {

    private Integer numericModelID;
    private Integer marketingModelNumber;
    private Integer modelYear;
    private String modelName;
    private String textIdentifier;
    private String travel;
    private String frontSuspension;
    private String rearSuspension;
    private FrontSpring frontSpring;
    private FrontRebound frontRebound;
    private FrontForSag frontForSag;
    private RearSpring rearSpring;
    private RearRebound rearRebound;
    private RearShockStroke rearShockStroke;
    private RearShockSagPSI rearShockSagPSI;
}
