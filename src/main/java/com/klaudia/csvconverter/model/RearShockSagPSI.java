package com.klaudia.csvconverter.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.LinkedHashMap;
import java.util.Map;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RearShockSagPSI {
    @Builder.Default
    private Map<String, String> rangeStringMap = new LinkedHashMap<>();
}
